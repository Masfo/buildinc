// AUTOMATICALLY GENERATED BY BUILDINC v0.2.197 TOOL
// LAST BUILD (CMAKE): 2022-09-30 14:26:29

#pragma once

#ifndef BUILDINCVERSION_BUILDNUMBER_HEADER_H
#define BUILDINCVERSION_BUILDNUMBER_HEADER_H

#include <cstdint>

namespace BuildIncVersion
{
		 // You can modify major and minor
		constexpr uint32_t major = 0;
		constexpr uint32_t minor = 2;

		 // Do not modify these
		constexpr uint32_t build = 198;
		
		constexpr uint32_t version = major * 10000 + minor * 1000 + build;
		constexpr uint64_t random_seed = 0xd7733aac6c6674a;
		
		constexpr char version_string[] = "v0.2.198";
		constexpr char build_time_string[] = "2022-09-30 14:26:29";
		constexpr char phrase[] = "enamel-cushy-expire";
		constexpr char calver[] = "2022.39.198";

		// Copy paste to import to your project
		/*
			constexpr auto major = BuildIncVersion::major;
			constexpr auto minor = BuildIncVersion::minor;
			constexpr auto build = BuildIncVersion::build;
			constexpr auto version = BuildIncVersion::version;
			constexpr auto random_seed = BuildIncVersion::random_seed;
			
			constexpr auto version_string = BuildIncVersion::version_string;
			constexpr auto build_time_string = BuildIncVersion::build_time_string;
			constexpr auto phrase = BuildIncVersion::phrase;
			constexpr auto calver = BuildIncVersion::calver;
		*/
}
#endif // BUILDINCVERSION_BUILDNUMBER_HEADER_H

